(ns plf02.core)


(defn función-associative?-1
  [coll]
  (associative? coll))

(defn función-associative?-2
  [coll]
  (associative? coll))

(defn función-associative?-3
  [coll]
  (associative? coll))

(función-associative?-1 [1 2 3])
(función-associative?-2 '(1 2 3))
(función-associative?-3 {:a 1 :b 2})

(defn función-boolean?-1
  [x]
  (boolean? x))

(defn función-boolean?-2
  [x]
  (boolean? x))

(defn función-boolean?-3
  [x]
  (boolean? x))

(función-boolean?-1 true)
(función-boolean?-2 false)
(función-boolean?-3 nil)

(defn función-char?-1
  [x]
  (char? x))

(defn función-char?-2
  [x]
  (char? x))

(defn función-char?-3
  [x]
  (char? x))

(función-char?-1 \a)
(función-char?-2 642)
(función-char?-3 (first "Conrado"))

(defn función-coll?-1
  [x]
  (coll? x))

(defn función-coll?-2
  [x]
  (coll? x))

(defn función-coll?-3
  [x]
  (coll? x))

(función-coll?-1 {:x 23 :y 34})
(función-coll?-2 [23 2 5 1 5])
(función-coll?-3 '(1 2 3))

(defn función-decimal?-1
  [n]
  (decimal? n))

(defn función-decimal?-2
  [n]
  (decimal? n))

(defn función-decimal?-3
  [n]
  (decimal? n))

(función-decimal?-1 1)
(función-decimal?-2 1.0)
(función-decimal?-3 99999999999999999999999999999999999)

(defn función-double?-1
  [x]
  (double? x))

(defn función-double?-2
  [x]
  (double? x))

(defn función-double?-3
  [x]
  (double? x))

(función-double?-1 1.0)
(función-double?-2 1.0M)
(función-double?-3 (new Double "1"))

(defn función-float?-1
  [x]
  (float? x))

(defn función-float?-2
  [x]
  (float? x))

(defn función-float?-3
  [x]
  (float? x))

(función-float?-1 0)
(función-float?-2 0.0)
(función-float?-3 0)

(defn función-ident?-1
  [x]
  (ident? x))

(defn función-ident?-2
  [x]
  (ident? x))

(defn función-ident?-3
  [x]
  (ident? x))

(función-ident?-1 'abc)
(función-ident?-2 :hello)
(función-ident?-3 123)

(defn función-indexed?-1
  [coll]
  (indexed? coll))

(defn función-indexed?-2
  [coll]
  (indexed? coll))

(defn función-indexed?-3
  [coll]
  (indexed? coll))

(función-indexed?-1 [23 2 5 1 5])
(función-indexed?-2 {:a 1 :b 2})
(función-indexed?-3 '(1 2 3 4))

(defn función-int?-1
  [x]
  (int? x))

(defn función-int?-2
  [x]
  (int? x))

(defn función-int?-3
  [x]
  (int? x))

(función-int?-1 42.0)
(función-int?-2 42)
(función-int?-3 (bigint 42))


(defn función-integer?-1
  [n]
  (integer? n))

(defn función-integer?-2
  [n]
  (integer? n))

(defn función-integer?-3
  [n]
  (integer? n))

(función-integer?-1 1.0)
(función-integer?-2 2)
(función-integer?-3 13N)

(defn función-keyword?-1
  [x]
  (keyword? x))

(defn función-keyword?-2
  [x]
  (keyword? x))

(defn función-keyword?-3
  [x]
  (keyword? x))

(función-keyword?-1 'x)
(función-keyword?-2 true)
(función-keyword?-3 :x)

(defn función-list?-1
  [x]
  (list? x))

(defn función-list?-2
  [x]
  (list? x))

(defn función-list?-3
  [x]
  (list? x))

(función-list?-1 '(1 2 3))
(función-list?-2 0)
(función-list?-3 (range 10))

(defn función-map-entry?-1
  [x]
  (map-entry? x))

(defn función-map-entry?-2
  [x]
  (map-entry? x))

(defn función-map-entry?-3
  [x]
  (map-entry? x))

(función-map-entry?-1 (first {:a 1 :b 2}))
(función-map-entry?-2 (first {:a 1 :b 2 :c 4}))
(función-map-entry?-3 (first {:a 2}))

(defn función-map?-1
  [x]
  (map? x))

(defn función-map?-2
  [x]
  (map? x))

(defn función-map?-3
  [x]
  (map? x))

(función-map?-1 {:a 1 :b 2 :c 3})
(función-map?-2 '(1 2 3))
(función-map?-3 #{:a :b :c})

(defn función-nat-int?-1
  [x]
  (nat-int? x))

(defn función-nat-int?-2
  [x]
  (nat-int? x))

(defn función-nat-int?-3
  [x]
  (nat-int? x))

(función-nat-int?-1 0)
(función-nat-int?-2 1)
(función-nat-int?-3 -1)

(defn función-number?-1
  [x]
  (number? x))

(defn función-number?-2
  [x]
  (number? x))

(defn función-number?-3
  [x]
  (number? x))

(función-number?-1 :a)
(función-number?-2 1)
(función-number?-3 "23")


(defn función-pos-int?-1
  [x]
  (pos-int? x))

(defn función-pos-int?-2
  [x]
  (pos-int? x))

(defn función-pos-int?-3
  [x]
  (pos-int? x))

(función-pos-int?-1 1)
(función-pos-int?-2 -1)
(función-pos-int?-3 0)

(defn función-ratio?-1
  [n]
  (ratio? n))

(defn función-ratio?-2
   [n]
  (ratio? n))

(defn función-ratio?-3
  [n]
  (ratio? n))

(función-ratio?-1 22/7)
(función-ratio?-2 22)
(función-ratio?-3 2.2)

(defn función-rational?-1
  [n]
  (rational? n))

(defn función-rational?-2
  [n]
  (rational? n))

(defn función-rational?-3
 [n]
  (rational? n))

(función-rational?-1 1)
(función-rational?-2 1.0)
(función-rational?-3 62)

(defn función-seq?-1
  [x]
  (seq? x))

(defn función-seq?-2
  [x]
  (seq? x))

(defn función-seq?-3
  [x]
  (seq? x))

(función-seq?-1 1)
(función-seq?-2 [1])
(función-seq?-3 (seq [1]))

(defn función-seqable?-1
  [x]
  (seqable? x))

(defn función-seqable?-2
  [x]
  (seqable? x))

(defn función-seqable?-3
  [x]
  (seqable? x))

(función-seqable?-1 nil)
(función-seqable?-2 "")
(función-seqable?-3 {})

(defn función-sequential?-1
  [x]
  (sequential? x))

(defn función-sequential?-2
  [x]
  (sequential? x))

(defn función-sequential?-3
  [x]
  (sequential? x))

(función-sequential?-1 [1 2 3])
(función-sequential?-2 (range 1 5))
(función-sequential?-3 '(1 2 3))


(defn función-set?-1
  [x]
  (set? x))

(defn función-set?-2
  [x]
  (set? x))

(defn función-set?-3
  [x]
  (set? x))

(función-set?-1 #{1 2 3})
(función-set?-2 {:a 1 :b 2})
(función-set?-3 [1 2 3])

(defn función-some?-1
  [x]
  (some? x))

(defn función-some?-3
  [x]
  (some? x))

(defn función-some?-3
  [x]
  (some? x))

(función-some?-1 nil)
(función-some?-2 42)
(función-some?-3 false)

(defn función-string?-1
  [x]
  (string? x))

(defn función-string?-2
  [x]
  (string? x))

(defn función-string?-3
  [x]
  (string? x))

(función-string?-1 "abc")
(función-string?-2 \a)
(función-string?-3 1)

(defn función-symbol?-1
  [x]
  (symbol? x))

(defn función-symbol?-2
  [x]
  (symbol? x))

(defn función-symbol?-3
  [x]
  (symbol? x))

(función-symbol?-1 'a)
(función-symbol?-2 1)
(función-symbol?-3 :a)

(defn función-vector?-1
  [x]
  (vector? x))

(defn función-vector?-2
  [x]
  (vector? x))

(defn función-vector?-3
  [x]
  (vector? x))

(función-vector?-1 [1 2 3])
(función-vector?-2 '(1 2 3))
(función-vector?-3 {:a 1 :b 2 :c 3})

(defn función-drop-1
  [n coll]
  (drop n coll))

(defn función-drop-2
  [n coll]
  (drop n coll))

(defn función-drop-3
[n coll]
  (drop n coll))

(función-drop-1 0 [1 2 3 4])
(función-drop-2 -1 [1 2 3 4])
(función-drop-3 2 [1 2 3 4])

(defn función-drop-last-1
  [n coll]
  (drop-last n coll))

(defn función-drop-last-2
  [n coll]
  (drop-last n coll))

(defn función-drop-last-3
  [n coll]
  (drop-last n coll))

(función-drop-last-1 [1 2 3 4])
(función-drop-last-2 2 (vector 1 2 3 4))
(función-drop-last-3 0 [1 2 3 4])

(defn función-drop-while-1
  [pred coll]
  (drop-while pred coll))

(defn función-drop-while-2
  [pred coll]
  (drop-while pred coll))

(defn función-drop-while-3
  [pred coll]
  (drop-while pred coll))

(función-drop-while-1 #(> 3 %) [1 2 3 4 5 6])
(función-drop-while-2 neg? [-1 -2 -6 -7 1 2 3 4 -5 -6 0 1])
(función-drop-while-3 neg? [-1 -6 -7 3 4 -5])

(defn función-every?-1
  [pred coll]
  (every? pred coll))

(defn función-every?-2
  [pred coll]
  (every? pred coll))

(defn función-every?-3
 [pred coll]
  (every? pred coll))

(función-every?-1 even? '(2 4 6))
(función-every?-2 {1 "one" 2 "two"} [1 2])
(función-every?-3 #{1 2} [1 2 3])

(defn función-filterv-1
  [pred coll]
  (filterv pred coll))

(defn función-filterv-2
  [pred coll]
  (filterv pred coll))

(defn función-filterv-3
  [pred coll]
  (filterv pred coll))

(función-filterv-1 even? (range 10))
(función-filterv-2 even? (range 5))
(función-filterv-3 even? (range 3))

(defn función-group-by-1
  [f coll]
  (group-by f coll))

(defn función-group-by-2
  [f coll]
  (group-by f coll))

(defn función-group-by-3
[f coll]
  (group-by f coll))

(función-group-by-1 count ["a" "as" "asd" "aa" "asdf" "qwer"])
(función-group-by-2 odd? (range 10))
(función-group-by-3 :user-id [{:user-id 1 :uri "/"}
                              {:user-id 2 :uri "/foo"}
                              {:user-id 1 :uri "/account"}])

(defn función-iterate-1
  [f x]
  (iterate f x))

(defn función-iterate-2
  [f x]
  (iterate f x))

(defn función-iterate-3
  [f x]
  (iterate f x))

(función-iterate-1 inc 5)
(función-iterate-2 inc 10)
(función-iterate-3 inc 2)

(defn función-keep-1
  [f coll]
  (keep f coll))

(defn función-keep-2
  [f coll]
  (keep f coll))

(defn función-keep-3
  [f coll]
  (keep f coll))

(función-keep-1 even? (range 1 10))
(función-keep-2 #(if (odd? %) %) (range 10))
(función-keep-3 odd? (range 10))

(defn función-keep-indexed-1
 [f coll]
  (keep-indexed f coll))

(defn función-keep-indexed-2
 [f coll]
  (keep-indexed f coll))

(defn función-keep-indexed-3
  [f coll]
  (keep-indexed f coll))

(función-keep-indexed-1 #(if (odd? %1) %2) [:a :b :c :d :e])
(función-keep-indexed-2 #(if (pos? %2) %1) [-9 0 29 -7 45 3 -8])
(función-keep-indexed-3 (fn [idx v] (if (pos? v) idx)) [-9 0 29 -7 45 3 -8])


(defn función-map-indexed-1
  [f coll]
  (map-indexed f coll))

(defn función-map-indexed-2
  [f coll]
  (map-indexed f coll))

(defn función-map-indexed-3
  [f coll]
  (map-indexed f coll))

(función-map-indexed-1 hash-map "foobar")
(función-map-indexed-2 vector "foobar")
(función-map-indexed-3 list [:a :b :c])

(defn función-mapcat-1
  [f & colls]
  (mapcat f & colls))

(defn función-mapcat-2
  [f & colls]
  (mapcat f & colls))

(defn función-mapcat-3
 [f & colls]
  (mapcat f & colls))

(función-mapcat-1 reverse [[3 2 1 0] [6 5 4] [9 8 7]])
(función-mapcat-2 reverse [[6 2 5 4] [1 1 0 0] [5 8]])
(función-mapcat-3 reverse [[1 4 5 6] [3 6 1]])


(defn función-mapv-1
  [f coll]
  (mapv f coll))

(defn función-mapv-2
  [f c1 c2]
  (mapv f c1 c2))

(defn función-mapv-3
  [f c1 c2]
  (mapv f c1 c2))

(función-mapv-1 inc [1 2 3 4 5])
(función-mapv-2 + [1 2 3] [4 5 6])
(función-mapv-3 + [1 2 3] (iterate inc 1))

(defn función-merge-with-1
  [f & maps]
  (merge-with f & maps))

(defn función-merge-with-2
  [f & maps]
  (merge-with f & maps))

(defn función-merge-with-3
  [f & maps]
  (merge-with f & maps))

(función-merge-with-1 into {"Lisp" ["Common Lisp" "Clojure"] "ML" ["Caml" "Objective Caml"]}
                           {"Lisp" ["Scheme"] "ML" ["Standard ML"]})
(función-merge-with-2 +
                      {:a 1  :b 2}
                      {:a 9  :b 98 :c 0})
(función-merge-with-3 union
                      {:a #{1 2 3},   :b #{4 5 6}}
                      {:a #{2 3 7 8}, :c #{1 2 3}})

(defn función-not-any?-1
  [pred coll]
  (not-any? pred coll))

(defn función-not-any?-2
  [pred coll]
  (not-any? pred coll))

(defn función-not-any?-3
  [pred coll]
  (not-any? pred coll))

(función-not-any?-1 odd? '(2 4 6))
(función-not-any?-2 odd? '(1 2 3))
(función-not-any?-3 nil? [true false false])


(defn función-not-every?-1
  [pred coll]
  (not-every? pred coll))

(defn función-not-every?-2
  [pred coll]
  (not-every? pred coll))

(defn función-not-every?-3
  [pred coll]
  (not-every? pred coll))

(función-not-every?-1 odd? '(1 2 3))
(función-not-every?-2 odd? '(1 3))
(función-not-every?-3 odd? '(1 2 3 4))

(defn función-partition-by-1
  [f coll]
  (partition-by f coll))

(defn función-partition-by-2
  [f coll]
  (partition-by f coll))

(defn función-partition-by-3
  [f coll]
  (partition-by f coll))

(función-partition-by-1 #(= 3 %) [1 2 3 4 5])
(función-partition-by-2 odd? [1 1 1 2 2 3 3])
(función-partition-by-3 even? [1 1 1 2 2 3 3])

(defn función-reduce-kv-1
  [f init coll]
  (reduce-kv f init coll))

(defn función-reduce-kv-2
  [f init coll]
  (reduce-kv f init coll))

(defn función-reduce-kv-3
  [f init coll]
  (reduce-kv f init coll))

(función-reduce-kv-1 #(assoc %1 %2) {} {:a 1 :b 3})
(función-reduce-kv-2 #(assoc %1 %3 %2 %4) {} {:a 1 :b 2 :c 3 d:4})
(función-reduce-kv-3 #(assoc %1 %3 %2) {} {:a 1 :b 2 :c 3})

(defn función-remove-1
  [pred coll]
  (remove pred coll))

(defn función-remove-2
  [pred coll]
  (remove pred coll))

(defn función-remove-3
  [pred coll]
  (remove pred coll))

(función-remove-1 pos? [1 -2 2 -1 3 7 0])
(función-remove-2 nil? [1 nil 2 nil 3 nil])
(función-remove-3 even? (range 10))

(defn función-reverse-1
  [s]
  (reverse s))

(defn función-reverse-2
  [s]
  (reverse s))

(defn función-reverse-3
  [s]
  (reverse s))

(función-reverse-1 "foo")
(función-reverse-2 "conrado")
(función-reverse-3 "josé")

(defn función-some-1
  [pred coll]
  (some pred coll))

(defn función-some-2
  [pred coll]
  (some pred coll))

(defn función-some-3
  [pred coll]
  (some pred coll))

(función-some-1 even? '(1 2 3 4))
(función-some-2 even? '(1 3 5 7))
(función-some-3 true? [false false false])

(defn función-sort-by-1
  [keyfn coll]
  (sort-by keyfn coll))

(defn función-sort-by-2
  [keyfn coll]
  (sort-by keyfn coll))

(defn función-sort-by-3
  [keyfn comp coll]
  (sort-by keyfn comp coll))

(función-sort-by-1 count ["aaa" "bb" "c"])
(función-sort-by-2 first [[1 2] [2 2] [2 3]])
(función-sort-by-3 first > [[1 2] [2 2] [2 3]])

(defn función-split-with-1
  [pred coll]
  (split-with pred coll))

(defn función-split-with-2
  [pred coll]
  (split-with pred coll))

(defn función-split-with-3
   [pred coll]
  (split-with pred coll))

(función-split-with-1 (partial >= 3) [1 2 3 4 5])
(función-split-with-2 (partial > 3) [1 2 3 2 1])
(función-split-with-3 (partial > 10) [1 2 3 2 1])


(defn función-take-1
  [n coll]
  (take n coll))

(defn función-take-2
  [n coll]
  (take n coll))

(defn función-take-3
  [n coll]
  (take n coll))

(función-take-1 3 '(1 2 3 4 5 6))
(función-take-2 3 [1 2 3 4 5 6])
(función-take-3 3 [1 2])

(defn función-take-last-1
  [n coll]
  (take-last n coll))

(defn función-take-last-2
  [n coll]
  (take-last n coll))

(defn función-take-last-3
[n coll]
  (take-last n coll))

(función-take-last-1 2 [1 2 3 4])
(función-take-last-2 2 [1 4])
(función-take-last-3 2 nil)

(defn función-take-nth-1
  [n coll]
  (take-nth n coll))

(defn función-take-nth-2
  [n coll]
  (take-nth n coll))

(defn función-take-nth-3
[n coll]
  (take-nth n coll))

(función-take-nth-1 2 (range 10))
(función-take-nth-2 0 (range 2))
(función-take-nth-3 -10 (range 2))

(defn función-take-while-1
  [pred coll]
  (take-while pred coll))

(defn función-take-while-2
  [pred coll]
  (take-while pred coll))

(defn función-take-while-3
  [pred coll]
  (take-while pred coll))

(función-take-while-1 neg? [-2 -1 0 1 2 3])
(función-take-while-2 neg? [0 1 2 3])
(función-take-while-3 neg? [])

(defn función-update-1
  [m k f]
  (update m k f))

(defn función-update-2
  [m k f x]
  (update m k f x))

(defn función-update-3
  [m k f x y]
  (update m k f x y))

(función-update-1 {:name "James" :age 26} :age inc)
(función-update-2 {:name "James" :age 26} :age + 10)
(función-update-3 {:name "James" :age 26} :correct #(if (= % "true") true false))

(defn función-update-in-1
  [m ks f & args]
  (update-in ks f & args))

(defn función-update-in-2
  [m ks f & args]
  (update-in ks f & args))

(defn función-update-in-3
  [m ks f & args]
  (update-in ks f & args))

(función-update-in-1 {:name "James" :age 26} [:age] inc)
(función-update-in-2 {:name "James" :age 26} [:age] + 10)
(función-update-in-3 {:name "James" :age 26} [:age] - 10)